package com.example.tutorial.entities;

import org.apache.tapestry5.beaneditor.Validate;

import com.example.tutorial.data.Honorific;

public class Address
{
    @Validate("required")
    public String firstName;
    
    public Honorific honorific;
    public String lastName;
    public String street1;
    public String street2;
    public String city;
    public String state;
    
    @Validate("required,regexp")
    public String zip;
    
    public String email;
    public String phone;
}